#!/bin/bash
 
#$ -S /bin/bash
#$ -N networksingle_array
 
#$ -o /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.out
#$ -e /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.err
#$ -m beas 
#$ -l h_rt=120:00:00
#$ -l h_vmem=16G
#$ -l highmem
#$ -binding linear:1

REGION="$1"
LINK="$2"
PLOT="$3"
module load R/3.4.3-1

echo "network task $SGE_TASK_ID ..."
Rscript 181111_deadwood_networks_single.R $SGE_TASK_ID $REGION $LINK $PLOT
echo "done"
