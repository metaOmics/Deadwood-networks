#!/bin/bash
 
#$ -S /bin/bash
#$ -N mod_arrayA
 
#$ -o /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.out
#$ -e /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.err
#$ -m beas 
#$ -l h_rt=170:00:00
#$ -l h_vmem=18G
#$ -l highmem
#$ -binding linear:1

REGION="$1"
LINK="$2"
WOOD="$3"

module load R/3.4.3-1

echo "stats task $SGE_TASK_ID ..."
Rscript 180830_deadwood_modules_split.R $SGE_TASK_ID $REGION $LINK $WOOD
echo "done"
