#!/bin/bash
 
#$ -S /bin/bash
#$ -N rarefy_array
#$ -o /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.out
#$ -e /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.err
#$ -m beas 
#$ -l h_rt=120:00:00
#$ -l h_vmem=12G
#$ -l highmem
#$ -binding linear:1

REGION="$1"

module load R/3.4.3-1

echo "rarefaction task $SGE_TASK_ID ..."
Rscript 180410_deadwood_rarefy_tables.R $SGE_TASK_ID $REGION
echo "done"
