#this script prepares R-objects with summarized tables of OTUs per treespecies/wood
#it uses the initial sample table and the rarefied OTU tables as input
# arguments are passed to choose the rarefaction depth, the gene region and the way links are set
# three sets of 1000 tables each are written to disk: 
## one with summaries per treespecies/wood for both wood types, 
## one with summaries per treespecies/wood for splint woods, 
## one with summaries per treespecies/wood for heart woods
# it works just like 180410_deadwood_networks, except for the filtering step 
# that removes from each rarefied OTU table the OTUs with less than 4 reads 

#read arguments passed from the shell script that calls R:
args<-commandArgs(TRUE)
depth <- as.numeric(args[1]) #depth from submit script
region <- args[2] #region (ITS or 16S)
link <-  args[3] # how to establish link

#no special libraries are needed

# network reconstruction: 
# links can be based on any co-occurence (mean or max - both will be >0, if any sample is >0, but mean and max differ in the strength),
# consistent co-occurence (min - is only >0, if all samples are >0), 
# 2 out of 3 detections (median - is 0 if 2 or 3 out of 3 values are 0)
# both woods, splint network, core network 
sampleD <- readRDS("sampleD.RDS")
# read data:
tab <- readRDS(paste("table",region,"rarefied",depth,"RDS",sep="."))

#names with treespecies and wood in the order used in the networks:
nAll <- aggregate(1:length(grep("_",sampleD$treespecies_depth)),
                  list(sampleD$treespecies_depth),sum)[,1] 
#names with treespecies in splint wood in the order used in the networks:
nSplint <- aggregate(1:length(grep("_splint",sampleD$treespecies_depth)),
                     list(sampleD$treespecies_depth[grep("_splint",sampleD$treespecies_depth)]),sum)[,1] 
#names with treespecies in heart wood in the order used in the networks:
nCore <- aggregate(1:length(grep("_core",sampleD$treespecies_depth)),
                   list(sampleD$treespecies_depth[grep("_core",sampleD$treespecies_depth)]),sum)[,1]

if(link == "median"){ # networks with 2 out of 3
  #networks for splint and heartwoods:
  net <- lapply(c(1:1000),function(z) { # "iterate" over 1000 versions from rarefaction
    tabt <- tab[[z]] #temporary OTU table for filtering
    # make all entries of lines that have less than 4 reads 0:
    tabt[rowSums(tabt)<4,] <- 0 # this way, the same OTUs are represented in all tables (useful for later comparisons)
    #make a table with the median reads per OTU per tree and wood type, this time with rows=sample groups/columns=OTUs:
    tmpn <- apply(tabt,1,function(x) aggregate(x,list(sampleD$treespecies_depth),
                                     function(y) median(y))[,2])
    rownames(tmpn) <- nAll #nicer sample names
    tmpn #return, because it's in an lapply loop
    })
  saveRDS(net,paste("networkDom.all",region,"rarefied",depth,"linkedBy",link,"RDS",sep=".")) #save networks
  print("splint and core: done")
  rm(net) #to avoid clutter
  #networks for heartwoods, same as before just with less data:
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    # make all entries of lines that have less than 4 reads 0:
    tabt[rowSums(tabt)<4,] <- 0
    #only on the heartwood samples:
    tmpn <- apply(tabt[,grep("_core",sampleD$treespecies_depth)],1, 
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_core",sampleD$treespecies_depth)]),
                                        function(y) median(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("networkDom.core",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("core done")
  rm(net)
  #networks for splintwoods, same as before just with less data:
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    # make all entries of lines that have less than 4 reads 0:
    tabt[rowSums(tabt)<4,] <- 0
    #only on the splintwood samples:
    tmpn <- apply(tabt[,grep("_splint",sampleD$treespecies_depth)],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_splint",sampleD$treespecies_depth)]),
                                        function(y) median(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("networkDom.splint",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("splint done")
}else if(link == "mean"){ #same here, just with the mean instead of median
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt,1,function(x) aggregate(x,list(sampleD$treespecies_depth),
                                                   function(y) mean(y))[,2])
    rownames(tmpn) <- nAll
    tmpn
  })
  saveRDS(net,paste("networkDom.all",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("splint and core: done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt[,grep("_core",sampleD$treespecies_depth)],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_core",sampleD$treespecies_depth)]),
                                        function(y) mean(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("networkDom.core",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("core done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt[,grep("_splint",sampleD$treespecies_depth)],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_splint",sampleD$treespecies_depth)]),
                                        function(y) mean(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("networkDom.splint",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("splint done")
}else if(link == "max"){
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt,1,function(x) aggregate(x,list(sampleD$treespecies_depth),
                                                   function(y) max(y))[,2])
    rownames(tmpn) <- nAll
    tmpn
  })
  saveRDS(net,paste("networkDom.all",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("splint and core: done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt[,grep("_core",sampleD$treespecies_depth)],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_core",sampleD$treespecies_depth)]),
                                        function(y) max(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("networkDom.core",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("core done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt[,grep("_splint",sampleD$treespecies_depth)],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_splint",sampleD$treespecies_depth)]),
                                        function(y) max(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("networkDom.splint",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("splint done")
}else if(link == "min"){
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt,1,function(x) aggregate(x,list(sampleD$treespecies_depth),
                                                   function(y) min(y))[,2])
    rownames(tmpn) <- nAll
    tmpn
  })
  saveRDS(net,paste("networkDom.all",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("splint and core: done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt[,grep("_core",sampleD$treespecies_depth)],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_core",sampleD$treespecies_depth)]),
                                        function(y) min(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("networkDom.core",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("core done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tabt <- tab[[z]]
    tabt[rowSums(tabt)<4,] <- 0
    tmpn <- apply(tabt[,grep("_splint",sampleD$treespecies_depth)],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[grep("_splint",sampleD$treespecies_depth)]),
                                        function(y) min(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("networkDom.splint",region,"rarefied",depth,"linkedBy",link,"RDS",sep="."))
  print("splint done")
}

