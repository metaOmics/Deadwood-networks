#this script calculates network-level statistics
#it uses the summarized OTU tables or nullmodel tables
# arguments are passed to choose the rarefaction depth, gene region, link type, wood groups and OTU table or null model

#path to the libraries on Eve, needs to be adjusted on other systems only:
.libPaths("/data/project/metaamp/TOOLS/Rlibs_3.4.3")

# we need bipartite, which depends on vegan
library(vegan)
library(bipartite)

# read arguments from shell script:
args<-commandArgs(TRUE)
depth <- as.numeric(args[1]) #depth from submit script
region <- args[2] #region (ITS or 16S) and also both
link <-  args[3] #link: median, mean, min, max
netw <- args[4] #woods in network: all splint core
nullm <- args[5] #OTU table: 0, nullmodels: method 1, 3, or 4

# construct input filename, either for OTU table or nullmodels, depending on shell script (only one per run):
print("reading")
if(nullm == "0"){
  # original summarized OTU table (set of 1000):
  netfile <- paste("network",netw,region,"rarefied",depth,"linkedBy",link,"RDS",sep=".")
}else{
  # nullmodel tables (set of 1000):
  netfile <- paste("network",netw,"nullmodel",nullm,region,"rarefied",depth,"linkedBy",link,"RDS",sep=".")
}

#read input file:
net <- readRDS(netfile)
#calculate modularity (function from bipartite package):
print("calculating modularity")
modl <- sapply(net,# applied here to all 1000 versions, returns a vector with one value per version
               function(x) {
	x <- x[,colSums(x)>0] # we use only the OTUs that are actually linked to anything
	computeModules(x)@likelihood
})
#all other stats are accessed via the networklevel function from bipartite
print("calculating other stats")
nobs <- sapply(net, # applied here to all 1000 versions, returns a matrix with one column per version, 
               function(x) {
	x <- x[,colSums(x)>0] # we use only the OTUs that are actually linked to anything
	networklevel(x,index = c("connectance", "web asymmetry","links per species","number of compartments",
                                                           "cluster coefficient","nestedness","weighted nestedness",
                                                           "weighted NODF","ISA","SA","linkage density","weighted connectance",
                                                           "interaction evenness","Alatalo interaction evenness","Shannon diversity","H2",
                                                           "number of species"))
})
# save results:
print("writing")
#saveRDS(nobs,gsub("network","statsN_pre",netfile))#remove if the next line works
saveRDS(rbind(nobs,modl),gsub("network","statsN",netfile)) # modularity and the rest are just glued together in the output
                                                            # the output name uses the input name, just replacing network by statsN
