#this script prepares R-objects with rarefied OTU tables
#it takes only the initial workspace as input
# arguments are passed to choose the rarefaction depth and the gene region

#path to the libraries on Eve, needs to be adjusted on other systems only:
.libPaths("/data/project/metaamp/TOOLS/Rlibs_3.4.3")

#we need vegan for the rarefaction
library(vegan)

#read arguments passed from the shell script that calls R:
args<-commandArgs(TRUE)
depth <- as.numeric(args[1]) #rarefaction depth from submit script
region <- args[2] #region (ITS or 16S)

#load workspace (WS2, with variation in rarefaction levels):
load("180430_deadwood_WS2.Rdata")

if(region == "ITS"){ # for call to run on ITS data
  otuITSR <- list() # initialize list for 1000 rarefactions of the ITS OTU table
  for(j in 1:1000){ #iterate over 1000 versions, the actual read numbers are in the list with variations
    otuITSR[[j]] <- t(rrarefy(t(otuITS),readsRITS[[k]][,depth])) #rarefy and bring back into rows=OTUs/columns=samples format
    print(j) #control iterations
  }
  saveRDS(otuITSR,paste("table",region,"varefied",depth,"RDS",sep=".")) #save list with 1000 OTU tables as RDS file
}else if(region == "16S"){ #same as above, except it uses 16S data objects
  otu16R <- list()
  for(j in 1:1000){ 
    otu16R[[j]] <- t(rrarefy(t(otu16),readsR16[[k]][,depth]))
    print(j)
  }
  saveRDS(otu16R,paste("table",region,"varefied",depth,"RDS",sep="."))
}
