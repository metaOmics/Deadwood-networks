#this script prepares R-objects with summarized tables of OTUs per treespecies/wood
#it uses the initial sample table and the rarefied OTU tables as input
# arguments are passed to choose the rarefaction depth, the gene region and the way links are set
# three sets of 1000 tables each are written to disk: 
## one with summaries per treespecies/wood for both wood types, 
## one with summaries per treespecies/wood for splint woods, 
## one with summaries per treespecies/wood for heart woods

#read arguments passed from the shell script that calls R:
args<-commandArgs(TRUE)
depth <- as.numeric(args[1]) #depth from submit script
region <- args[2] #region (ITS or 16S)
link <-  args[3] # how to establish link #this is not really meaningful here
plotn <- args[4] # 1-3, coding the plots

#read sample data - which sample is which tree
sampleD <- readRDS("sampleD.RDS")
# read rarefied OTU table
tab <- readRDS(paste("table",region,"rarefied",depth,"RDS",sep="."))

# networks will be made for both woods, splint network, core network:
# prettier names, see 180410_deadwood_networks for details
nAll <- aggregate(1:length(grep("_",sampleD$treespecies_depth)),
                  list(sampleD$treespecies_depth),sum)[,1]
nSplint <- aggregate(1:length(grep("_splint",sampleD$treespecies_depth)),
                     list(sampleD$treespecies_depth[grep("_splint",sampleD$treespecies_depth)]),sum)[,1]
nCore <- aggregate(1:length(grep("_core",sampleD$treespecies_depth)),
                   list(sampleD$treespecies_depth[grep("_core",sampleD$treespecies_depth)]),sum)[,1]

if(link == "median"){# all links (median, mean, min, max) will do the same, so ignore this
  #for a network with both splint- and heartwood:
  net <- lapply(c(1:1000),function(z) { # "iterate" over the rarefaction versions
    tmpt <- tab[[z]][,sampleD$Plot==plotn] #use only the one plot
    #we still summarize as before, to conserve the sample order, and we only use OTUs with at least 4 reads in the data set of this plot:
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn]),
                                                                         function(y) median(y))[,2])
    rownames(tmpn) <- nAll #rename with the pretty names
    tmpn #return
  })
  saveRDS(net,paste("network.all",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep=".")) #save networs (1000)
  print("splint and core: done")
  rm(net)
  #for a network with both only heartwood:
  net <- lapply(c(1:1000),function(z) {
    # get only the heartwood samples from the plot of interest:
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])] 
    # here we use only OTUs with at least 4 reads in the data set from this plot:
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) median(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("network.core",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))#save core networks
  print("core done")
  rm(net)
  #for a network with both only splintwood:
  net <- lapply(c(1:1000),function(z) {
    # get only the splintwood samples from the plot of interest:
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]
    # here we use only OTUs with at least 4 reads in the data set from this plot:
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) median(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("network.splint",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep=".")) #save splint networks
  print("splint done")
}else if(link == "mean"){ # all links (median, mean, min, max) will do the same, so ignore the rest of the script
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn]),
                                                                         function(y) mean(y))[,2])
    rownames(tmpn) <- nAll
    tmpn
  })
  saveRDS(net,paste("network.all",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("splint and core: done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) mean(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("network.core",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("core done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) mean(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("network.splint",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("splint done")
}else if(link == "max"){
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn]),
                                                                         function(y) max(y))[,2])
    rownames(tmpn) <- nAll
    tmpn
  })
  saveRDS(net,paste("network.all",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("splint and core: done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) max(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("network.core",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("core done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) max(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("network.splint",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("splint done")
}else if(link == "min"){
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn]),
                                                   function(y) min(y))[,2])
    rownames(tmpn) <- nAll
    tmpn
  })
  saveRDS(net,paste("network.all",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("splint and core: done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_core",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) min(y))[,2])
    rownames(tmpn) <- nCore
    tmpn
  })
  saveRDS(net,paste("network.core",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("core done")
  rm(net)
  net <- lapply(c(1:1000),function(z) {
    tmpt <- tab[[z]][,sampleD$Plot==plotn][,grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]
    tmpn <- apply(tmpt[rowSums(tmpt)>3,],1,
                  function(x) aggregate(x,list(sampleD$treespecies_depth[sampleD$Plot==plotn][grep("_splint",sampleD$treespecies_depth[sampleD$Plot==plotn])]),
                                        function(y) min(y))[,2])
    rownames(tmpn) <- nSplint
    tmpn
  })
  saveRDS(net,paste("network.splint",region,"rarefied",depth,"linkedBy",link,"plot",plotn,"RDS",sep="."))
  print("splint done")
}

