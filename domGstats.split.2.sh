#!/bin/bash
 
#$ -S /bin/bash
#$ -N domgstats_arrayB
 
#$ -o /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.out
#$ -e /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.err
#$ -m beas 
#$ -l h_rt=120:00:00
#$ -l h_vmem=18G
#$ -l highmem
#$ -binding linear:1

REGION="$1"
LINK="$2"
WOOD="$3"
MODEL="$4"

module load R/3.4.3-1

echo "stats task $SGE_TASK_ID ..."
Rscript 181002_deadwood_stats_groupsDom_split2.R $SGE_TASK_ID $REGION $LINK $WOOD $MODEL
echo "done"
