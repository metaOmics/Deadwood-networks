#this script calculates network statistics for each trophic level (trees and OTUs)
#it uses the summarized OTU tables or nullmodel tables
# arguments are passed to choose the rarefaction depth, gene region, link type, wood groups and OTU table or null model
# this script is exactly like 180410_deadwood_stats_groups.R, except for the name of the input and output file. 
# please refer to that script for comments

#path to the libraries on Eve, needs to be adjusted on other systems only:
.libPaths("/data/project/metaamp/TOOLS/Rlibs_3.4.3")

library(vegan)
library(bipartite)
library(igraph)

args<-commandArgs(TRUE)
depth <- as.numeric(args[1]) #depth from submit script
region <- args[2] #region (ITS or 16S)
link <-  args[3]
netw <- args[4] # all splint core
nullm <- 0 #args[5] # 0 1 3 4 # this script is not used for nullmodels, because they were not calculated for the varefied datasets

# construct input filename, either for OTU table or nullmodels, depending on shell script (only one per run),
# this is the only thing that differs from 180410_deadwood_stats_groups.R:
print("reading")
if(nullm == "0"){
  netfile <- paste("network",netw,region,"varefied",depth,"linkedBy",link,"RDS",sep=".")
}else{
  netfile <- paste("network",netw,"nullmodel",nullm,region,"varefied",depth,"linkedBy",link,"RDS",sep=".")
}

net <- readRDS(netfile)
print("calculating stats")
nobs <- sapply(net,function(x) grouplevel(x,index = c("number of species","mean number of links","mean number of shared partners",
                                                           "togetherness","C score","V ratio","discrepancy","niche overlap",
                                                           "generality","vulnerability","fc")))
print("writing")
saveRDS(nobs,gsub("network","statsG",netfile)) 
