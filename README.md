# Bipartite occurence networks and their analysis

This repository harbours the scripts used to construct bipartite (co-)occurence networks for prokaryotic and fungal inhabitants of deadwood logs. This Readme summarizes 1) the R-scripts and 2) the shell scripts used to run the R-workflow to produce the results discussed in the manuscript. It further links to R-scripts which can perform very similar analyses, e.g. using a slightly different rarefaction approach, analysing only the dominant OTUs, or taking unreplicated OTU tables as input or combining the 16S- and ITS-based networks. 3) The R-script to analyse the results of the workflow is commented here.


## Part 1) The R scripts
The workflow is divided into several steps, most of which are run on the HPC. Overall it uses the following packages:

```
library(RColorBrewer)
library(broman)
library(vegan)
library(bipartite)
library(igraph)
```

#### Making the workspaces: 
We need five inputs, an OTU table for the ITS data set, an OTU table for the 16S data set, a taxonomy table for both, and a table describing the sample characteristics. All necessary further data is created based on these in the first script ([```180410_deadwood_makeWS1.R```](180410_deadwood_makeWS1.R)). This is the only step that was run locally.

***Variations***: The difference between [```180410_deadwood_makeWS1.R```](180410_deadwood_makeWS1.R) and [```180430_deadwood_makeWS2.R```](180430_deadwood_makeWS2.R) is that the former makes a table with one number of reads to rarefy each sample to in order to get a certain rarefaction level. The latter gives 1000 numbers of reads that vary according to the uncertainty in the estimation of the real richness in each sample. For the former, there are also more (and different between the target sequences) rarefaction levels. **The unvarying rarefaction was used in the manuscript.**

#### Rarefaction of the OTU tables:
Building on the workspaces output by [```180410_deadwood_makeWS1.R```](180410_deadwood_makeWS1.R), [```180410_deadwood_rarefy_tables.R```](180410_deadwood_rarefy_tables.R) performs the rarefactions according to the values and OTU tables in the workspaces. To decrease computational load, the script is called once per target region (16S, ITS) and rarefaction level - e.g. 10 times for 5 rarefaction levels. This way, the rarefaction level can be easily parsed from the array job number on the HPC. **The final numbers in the manuscript are from a rarefaction level of 29% of the predicted richness.** 

***Variations***: [```180430_deadwood_rarefy_tables_v.R```](180430_deadwood_rarefy_tables_v.R) does the same thing as [```180410_deadwood_rarefy_tables.R```](180410_deadwood_rarefy_tables.R), but the values in the workspace output from [```180430_deadwood_makeWS2.R```](180430_deadwood_makeWS2.R) vary and the format is slightly different.

*Excursion*: The rarefaction level is encoded as numbers starting from 1 up to 15 for the original 16S data set and 1 up to 5 for the variation version. The numbers then internally are converted to similar levels (e.g. 14 for ITS = 29%, 9 for 16S = 29%, 5 for variation = 29%):
```
ITS task no   ITS depth   16S depth   16S task no   variation task no
1             3				
2             5           5           1             1
3             7           8           2		
4             9				
5             11          11          3             2
6             13          14          4		
7             15				
8             17          17          5             3
9             19          20          6		
10            21				
11            23          23          7             4
12            25          26          8		
13            27				
14            29          29          9             5
15            31          32          10		
                          35          11		
                          38          12		
                          41          13		
                          44          14		
                          47          15		
                          50          16		
```

#### Network reconstruction from the rarefied OTU tables:
Based on the rarefied OTU tables from the previous steps, [```180410_deadwood_networks.R```](180410_deadwood_networks.R) prepares tables summarizing samples for the networks. The bipartite package uses these as input. To reduce computational load, the script is run separately for each rarefaction level, gene region and sort of link. Hence, the scripts take 3 arguments: depth (again coded as above), region (16S, ITS) and link (how/when a link should be set). In the publication, the link setting **median** was used: **links were set to the median value (i.e. if at 2 out of 3 replicate samples detected an OTU)**. The output for each run of the script are three sets of summarized tables, for all treespecies and wood types, for only splint wood and for only heart wood. **In the manuscript, the separate splint wood and heart wood tables were used**

***Variations***: 

- [```180430_deadwood_networks_v.R```](180430_deadwood_networks_v.R) for varying rarefaction: The only difference between the two scripts are the output names. [```180827_deadwood_combine_networks.R```](180827_deadwood_combine_networks.R) sticks the rarefied 16S and ITS OTU tables together prior to preparing the summarized OTU (network) tables. The links work exactly like in the other scripts.
- [```181001_deadwood_domNetworks.R```](181001_deadwood_domNetworks.R) does the same thing as [```180410_deadwood_networks.R```](180410_deadwood_networks.R), but it filters the datasets for OTUs that are represented by at least four reads ("dominant OTUs").
- [```181111_deadwood_networks_single.R```](181111_deadwood_networks_single.R) is yet another version, which was written to transform OTU tables with just one sample per wood type (from one plot) into the format used by bipartite. The link type is not really used here (any number of reads per OTU per sample will set a link), but to make sure we are not using too many spurious OTUs, only the OTUs with at least 4 reads in the dataset from that particular plot and woodtype are used (similar to the dominant version). Obviously, an additional argument for the plot number is expected.

#### Nullmodels for the networks:
Based on the summarized OTU tables, the bipartite package is used in [```180410_deadwood_nullmodels.R```](180410_deadwood_nullmodels.R) to make null model network tables. There are **three types of null-models, all of which were included in the manuscript: Patefield's algorithm, Vazquez' (swap) algorithm and a shuffle algorithm** (all part of the bipartite package). They are all calculated in the same run of script. (The script was later taken apart to run the very slow swap algorithm on its own, but the combined script is a better starting point.) The script is run per rarefaction level, gene region, sort of link and group of wood samples. The output is again an R object with a 1000-item list with null model tables, one object per algorithm. As there are already 1000 rarefactions, only one null model of each kind is built per rarefaction, i.e. there are finally 1000 networks and 1000 swap null models, 1000 shuffle null models and 1000 Patefield null models.

***Variation***: There is only one script, because the other varied rarefaction was not controlled by null models, since the output was very similar to the previously run normal rarefaction. Similarly, no nullmodels were made for the networks based on single plots or only OTUs with >3 reads. The combined networks of 16S and ITS data conveniently has the same naming convention as the other scripts, so in addition to '16S' and 'ITS', 'both' can be used to analyse the combined networks.

#### Network statistics:
Network characteristics are calculated at three levels: overall network, per trophic level (group), per tree/OTU. For each level, there is one script, ie. [```180410_deadwood_stats_groups.R```](180410_deadwood_stats_groups.R), [```180410_deadwood_stats_network.R```](180410_deadwood_stats_network.R) and [```180410_deadwood_stats_specs.R```](180410_deadwood_stats_specs.R). Each script is run separately for each rarefaction level, region, link type, wood group and OTU table or nullmodel, to reduce computation power per run. **The results of the overall network and per-trophic level are used in the manuscript**. The scripts for network and trohic levels return one simple matrix per run, with columns for every rarefied version. The species-level script returns lists of matrices. The species-level script is only run on the summarized OTU tables and not the nullmodels, because the nullmodels don't conserve the identity of the OTUs or trees.

***Variations***: 

- For the other type of rarefaction, [```180430_deadwood_stats_v_groups.R```](180430_deadwood_stats_v_groups.R), [```180430_deadwood_stats_v_network.R```](180430_deadwood_stats_v_network.R), and [```180509_deadwood_stats_v_specs.R```](180509_deadwood_stats_v_specs.R) do exactly the same thing. 
- Like above, for the combined networks of 16S and ITS data, 'both' can be used to analyse the combined networks.
- For the networks based only on OTUs with more than three reads, [```181002_deadwood_stats_networkDom_split.R```](181002_deadwood_stats_networkDom_split.R) and [```181002_deadwood_stats_groupDom_split.R```](181002_deadwood_stats_groupDom_split.R) were adapted. They don't differ from the original except for the names of input and output files. There were two versions made of them to facilitate faster calculations - each of the scripts only works with 500 of the 1000 rarefied versions. The way this is coded is a bit clumsy and should be revised if this kind of analysis becomes the norm. 
- To analyse the single-plot networks, [```181111_deadwood_stats_networkSingle_split.R```](181111_deadwood_stats_networkSingle_split.R) and [```181111_deadwood_stats_groupsSingle_split.R```](181111_deadwood_stats_groupsSingle_split.R) were adapted. These also only differs in the input and output names. They are also  made to use only 500 of 1000 versions.

#### Extraction of modules:
For the networks based on OTU tables (not nullmodels), modules were extracted using [```180830_deadwood_modules_split.R```](180830_deadwood_modules_split.R). The 'split' refers to the separate handling of packs of 500 of the 1000 rarefaction versions. This speeds up the calculations, however the way this is coded is a bit clumsy and should be revised if this kind of analysis becomes the norm. 

***Variation***: this was only performed for the main data set.


## Part 2) The shell scripts

As most of the analysis steps are run on an HPC, there are a number of shell scripts which trigger each step. The only step that was run locally is the setup of the initial workspace. It could also run on an HPC, but the local environment gave better control on the manual steps like colour picking.

All shell scripts have the same structure: they start with a bash shebang, then enlist submission arguments for the SGE queuing system (starting with #$ and referencing the job-ID and task-ID of array jobs), take command line arguments and load a module to have a suitable version of R. They typically echo their purpose and then call an R-script. The number of the array job is passed to the R-script. In the case of this project, it is always used to define the rarefaction depth (as explained above). The command line arguments are also passed straight on to R.

Theoretically, all steps in the shell scripts could be run one after the other, or all jobs submitted centrally. Here, they are listed in a sensible order:

- [```runRarefaction.sh```](runRarefaction.sh): runs the [```180410_deadwood_rarefy_tables.R```](180410_deadwood_rarefy_tables.R) script. It would be submitted by 
```
qsub -cwd -m beas -t 1-12 ./runRarefaction.sh 16S
```
or 
```
qsub -cwd -m beas -t 1-15 ./runRarefaction.sh ITS
``` 
Similarly, [```runVarefaction.sh```](runVarefaction.sh) would run the [```180430_deadwood_rarefy_tables_v.R```](180430_deadwood_rarefy_tables_v.R) script with the varying rarefaction. Since it is done with the same parameters in 16S and ITS datasets, it could be submitted by 
```
for REGION in 16S ITS; do qsub -cwd -m beas -t 1-5 ./runVarefaction.sh $REGION; done
```
- [```runNetworkReconstruction.sh```](runNetworkReconstruction.sh): runs the [```180410_deadwood_networks.R```](180410_deadwood_networks.R) script to summarize and format OTU tables for use in bipartite. It would be submitted by 
```
qsub -cwd -m beas -t 1-12 ./runNetworkReconstruction.sh 16S median
```
or 
```
qsub -cwd -m beas -t 1-15 ./runNetworkReconstruction.sh ITS median
```
(Obviously, "min", "max", or "mean" could be passed instead of "median".) Variations of this script are [```runNetworkReconstruction_v.sh```](runNetworkReconstruction_v.sh), [```runNetworkReconstructionSingle.sh```](runNetworkReconstructionSingle.sh), [```runNetworkDom.sh```](runNetworkDom.sh) and [```runNetworkCombination.sh```](runNetworkCombination.sh). They call the respective R-scripts to make summarized and formatted OTU tables for bipartite based on the alternative rarefaction, from single-plot datasets, dominant OTUs or by combining 16S and ITS data sets. All of these would be called in the much the same way: 
```
for REGION in 16S ITS; do qsub -cwd -m beas -t 1-5 ./runNetworkReconstruction_v.sh $REGION median; done
```
```
for PLOT in 1 2 3; do qsub -cwd -m beas -t 1-12 ./runNetworkReconstructionSingle.sh 16S median $PLOT; done
```
```
for PLOT in 1 2 3; do qsub -cwd -m beas -t 1-15 ./runNetworkReconstructionSingle.sh ITS median $PLOT; done
```
```
qsub -cwd -m beas -t 1-12 ./runNetworkDom.sh 16S median
```
```
qsub -cwd -m beas -t 1-15 ./runNetworkDom.sh ITS median
```
```
qsub -cwd -m beas -t 1-12 ./runNetworkCombination.sh median
```
- [```runNullmodels.sh```](runNullmodels.sh) runs [```180410_deadwood_nullmodels.R```](180410_deadwood_nullmodels.R) to build nullmodels. The script takes and passes on the types of wood the network is built on in addition to the previous parameters, like so:
```
for WOOD in all splint core; do qsub -cwd -m beas -t 1-12 ./runNullmodels.sh 16S median $WOOD; done
```
or 
```
for WOOD in all splint core; do qsub -cwd -m beas -t 1-15 ./runNullmodels.sh ITS median $WOOD; done
```
or 
```
for WOOD in all splint core; do qsub -cwd -m beas -t 1-5 ./runNullmodels.sh both median $WOOD; done
```
- [```runNstats.sh```](runNstats.sh) calls the R-script [```180410_deadwood_stats_network.R```](180410_deadwood_stats_network.R). It takes and passes on the gene region, the link type, the wood types and the nullmodel type (0 for original networks):
```
for MODEL in 0 1 3 4; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-12 ./runNstats.sh 16S median $WOOD $MODEL; done; done
```
or 
```
for MODEL in 0 1 3 4; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-15 ./runNstats.sh ITS median $WOOD $MODEL; done; done
```
or 
```
for MODEL in 0 1 3 4; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-5 ./runNstats.sh both median $WOOD $MODEL; done; done
```
Analogously, [```runN.v.sh```](runN.v.sh), which calls the R-script for the alternative rarefaction, is called by:
```
for WOOD in all splint core; do for REGION in 16S ITS; do qsub -cwd -m beas -t 1-5 ./runN.v.sh $REGION median $WOOD; done; done
```
(the type of nullmodel is not given, because we were not making and using null models here). Similarly, scripts for single plots and dominant OTUs are invoked by:
```
for SPLIT in 1 2; do for WOOD in all splint core; do for PLOT in 1 2 3; do qsub -cwd -m beas -t 1-12 ./singleNstats.split.$SPLIT.sh 16S median $WOOD 0 $PLOT; done; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do for PLOT in 1 2 3; do qsub -cwd -m beas -t 1-15 ./singleNstats.split.$SPLIT.sh ITS median $WOOD 0 $PLOT; done; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-12 ./domNstats.split.$SPLIT.sh 16S median $WOOD 0; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-15 ./domNstats.split.$SPLIT.sh ITS median $WOOD 0; done; done
```
- [```runGstats.sh```](runGstats.sh) starts [```180410_deadwood_stats_groups.R```](180410_deadwood_stats_groups.R) with the same arguments. 
```
for MODEL in 0 1 3 4; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-12 ./runGstats.sh 16S median $WOOD $MODEL; done; done
```
or 
```
for MODEL in 0 1 3 4; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-15 ./runGstats.sh ITS median $WOOD $MODEL; done; done
```
or 
```
for MODEL in 0 1 3 4; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-5 ./runGstats.sh both median $WOOD $MODEL; done; done
```
Of course, the variations can be similarly called:
```
for WOOD in all splint core; do for REGION in 16S ITS; do qsub -cwd -m beas -t 1-5 ./runG.v.sh $REGION median $WOOD; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do for PLOT in 1 2 3; do qsub -cwd -m beas -t 1-12 ./singleGstats.split.$SPLIT.sh 16S median $WOOD 0 $PLOT; done; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do for PLOT in 1 2 3; do qsub -cwd -m beas -t 1-15 ./singleGstats.split.$SPLIT.sh ITS median $WOOD 0 $PLOT; done; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-12 ./domGstats.split.$SPLIT.sh 16S median $WOOD 0; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-15 ./domGstats.split.$SPLIT.sh ITS median $WOOD 0; done; done
```
- [```runModules.split.1.sh```](runModules.split.1.sh) and [```runModules.split.2.sh```](runModules.split.2.sh) trigger module extraction. They are not used for the null-models and therefore don't take the model parameter:
```
for SPLIT in 1 2; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-12 ./runModules.split.$SPLIT.sh 16S median $WOOD; done; done
```
```
for SPLIT in 1 2; do for WOOD in all splint core; do qsub -cwd -m beas -t 1-15 ./runModules.split.$SPLIT.sh ITS median $WOOD; done; done
```

## 3) Comparison and plotting in R
