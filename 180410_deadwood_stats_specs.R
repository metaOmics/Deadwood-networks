#this script calculates network statistics for each tree and each OTUs
#it uses the summarized OTU tables
# arguments are passed to choose the rarefaction depth, gene region, link type and wood groups

#path to the libraries on Eve, needs to be adjusted on other systems only:
.libPaths("/data/project/metaamp/TOOLS/Rlibs_3.4.3")

library(vegan)
library(bipartite)

# read arguments from shell script:
args<-commandArgs(TRUE)
depth <- as.numeric(args[1]) #depth from submit script
region <- args[2] #region (ITS or 16S and also both)
link <-  args[3] #link: median, mean, min, max
netw <- args[4] #woods in network: all splint core
# the script is only run on the original summarized OTU table, because the nullmodels don't conserve tree or OTU identity

# construct input filename for OTU table, depending on shell script (only one per run):
print("reading")
netfile <- paste("network",netw,region,"rarefied",depth,"linkedBy",link,"RDS",sep=".")

#read network tables:
net <- readRDS(netfile)
#calculate stats for each OTU, using the specieslevel function of the bipartite package,
#again it is applied to the list of 1000 versions, so it returns a table for each of these
print("calculating stats for OTUs")
otures <- sapply(net,function(x) specieslevel(x,index = c("degree","species strength","nestedrank","interaction push pull","PDI",
                                                               "resource range","species specificity","PSI","betweenness","closeness",
                                                               "effective partners","proportional generality",
                                                               "proportional similarity","d"),level="higher"))
#save results for OTUs
print("writing otu results")
saveRDS(otures,gsub("network","statsS.otus",netfile)) 
rm(otures) # to avoid clutter
#calculate stats for each tree, using the specieslevel function of the bipartite package,
#again it is applied to the list of 1000 versions, so it returns a table for each of these
print("calculating stats for OTUs") #this is wrong, of course, should be "trees"
treeres <- sapply(net,function(x) specieslevel(x,index = c("degree","species strength","nestedrank","interaction push pull","PDI",
                                                          "resource range","species specificity","PSI","betweenness","closeness",
                                                          "effective partners","proportional generality",
                                                          "proportional similarity","d"),level="lower"))
#save results:
print("writing tree results")
saveRDS(treeres,gsub("network","statsS.trees",netfile)) 
