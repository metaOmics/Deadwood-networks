#!/bin/bash
 
#$ -S /bin/bash
#$ -N networks_array
 
#$ -o /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.out
#$ -e /work/$USER/deadwood/logs/$JOB_NAME-$JOB_ID-$TASK_ID.err
#$ -m beas 
#$ -l h_rt=120:00:00
#$ -l h_vmem=18G
#$ -l highmem
#$ -binding linear:1

LINK="$1"

module load R/3.4.3-1

echo "network task $SGE_TASK_ID ..."
Rscript 180827_deadwood_combine_networks.R $SGE_TASK_ID $LINK
echo "done"
